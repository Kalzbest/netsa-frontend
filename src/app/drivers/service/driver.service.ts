import {Injectable} from '@angular/core';
import {AppService} from "./../../service";

@Injectable()
/**
 * DriverService
 */
export class DriverService {

  private _path: string = "/api/Drivers";

  constructor(private _appService: AppService) {

  }

  createDriver(driver) {
    return this._appService.post(this._path, driver);
  }

  getDrivers() {
    return this._appService.get(this._path);
  }

  getDriver(id) {
    return this._appService.get(`${this._path}/${id}`);
  }

  updateDriver(driver) {
    return this._appService.put(`${this._path}`, driver);
  }

  deleteDriver(driver){
    return this._appService.delete(`${this._path}/${driver.id}`);
  }
}
