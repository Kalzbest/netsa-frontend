import { Component } from '@angular/core';
import { provideForms, disableDeprecatedForms,
          FormGroup, FormControl, REACTIVE_FORM_DIRECTIVES,
          Validators, FormBuilder } from '@angular/forms';
import { Router, ROUTER_DIRECTIVES } from '@angular/router';
import { HTTP_PROVIDERS } from '@angular/http';

import { Driver } from '../models';
import { AppService } from '../../service'
import { DriverService } from '../service'

@Component({
  directives: [REACTIVE_FORM_DIRECTIVES, ROUTER_DIRECTIVES],
  template: `
    <h1>Create Driver</h1>
    <form [formGroup]="driverForm" (submit)="createDriver()" novalidate>
    <input type="text" placeholder="name"
           formControlName="firstName"
           [(ngModel)]="driver.firstName">
    <br>
<button type="submit">Done</button>
</form>
<span (click)="driverService()">Create</span>
<a href="" (click)="createDriver()">sdafgd</a>
    <pre>
    {{ driver | json }}
</pre>

  `,
  providers: [
    disableDeprecatedForms(),
    provideForms(),
    HTTP_PROVIDERS,
      AppService,
      DriverService
  ]
})
export class DriverFormComponent {

  driverForm: FormGroup;

  private driver: Driver = new Driver();
  router: Router;
  constructor(fb: FormBuilder, router: Router, private driverService: DriverService) {
    this.driverForm = fb.group({
      firstName: ['']
    });
    console.log(this.driverForm);
  }

  createDriver() {
    this.driver = this.driverForm.value;
    // console.log(this.driver.firstName);
    // console.log(JSON.stringify(this.driverForm.value));
    this.driverService.createDriver(this.driver)
        .subscribe(res => console.log(res));
  }

}
