import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HTTP_PROVIDERS } from '@angular/http';

import { AppService } from '../../service'
import { DriverService } from '../service'
import {Driver} from "../models/driver";

@Component({
  template: `
    <h1>Driver Detail</h1>
    <h4>{{driver.firstName}}</h4>
    
`,
  providers: [
    HTTP_PROVIDERS, DriverService, AppService
  ]
})
export class DriverDetailComponent {

  private id: string;
  private driver: Driver = new Driver();

  constructor(private router: ActivatedRoute, private driverService: DriverService){
    this.router.params.forEach((params: Params) => {
      this.id = params['id'];
      console.log(this.id);
      this.driverService.getDriver(this.id)
        .subscribe(driver =>{
          this.driver = driver;
          console.log(driver.firstName);
        })
    });
  }



}
