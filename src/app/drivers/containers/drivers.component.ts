import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { DriversListComponent } from '../UI';
import { AppService } from '../../service';
import { DriverService } from '../service';
import {Driver} from "../models/driver";

@Component({
  selector: 'drivers',
  template: `
    <drivers-list [drivers]="drivers" (delete)="deleteDriver($event)"></drivers-list>
  `,
  directives: [
    DriversListComponent,
    ROUTER_DIRECTIVES
  ],
  providers: [
    AppService, DriverService
  ]
})

export class DriversComponent {
  drivers = [];

  constructor(private driverService: DriverService) {
    driverService.getDrivers()
      .subscribe(drivers =>{
        this.drivers = drivers;
        console.log(drivers);
      })
  }

  deleteDriver(driver: Driver) {
    this.driverService.deleteDriver(driver)
        .subscribe(res => {
          let index = this.drivers.indexOf(driver);
          alert(" deleted.\n" + index);
          this.drivers.splice(index, 1);

        })
  }

}
