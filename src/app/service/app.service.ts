import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/observable/throw';
import {Driver} from "../drivers/models/driver";

@Injectable()
/**
 * AppService
 */
export class AppService {

  headers: Headers = new Headers({
    'content-type': "application/json",
    Accept: "application/json"
  });

  private _url = "http://0.0.0.0:4000";

  constructor(private _http: Http) {

  }

  private getJson(res: Response) {
    return res.json();
  }

  private checkForError(res: Response): Response {
    if (res.status >= 200 && res.status < 300){
      return res;
    }else {
      var error = new Error(res.statusText);
      error['response'] = res;
      console.log(error);
      throw error;
    }
  }

  public get(path: string): Observable<any> {
    return this._http.get(`${this._url}${path}`, {headers: this.headers})
      .map(this.checkForError)
      .catch(err => Observable.throw(err))
      .map(this.getJson);
  }

  public post(path: string, body): Observable<any> {
    return this._http.post(
      `${this._url}${path}`,
      JSON.stringify(body),
      {headers: this.headers})
      .map(this.checkForError)
      .catch(err => Observable.throw(err))
      .map(this.getJson);
  }

  public put(path: string, body: Driver): Observable<any> {
    // return this._http.put(
    //   `${this._url}${path}`,
    //   JSON.stringify(body),
    //   {headers: this.headers})
    //   .map(this.checkForError)
    //   .catch(err => Observable.throw(err))
    //   .map(this.getJson);
    return this._http.post(`${this._url}${path}`, JSON.stringify(body)).map(this.getJson);
  }

  public delete(path: string): Observable<any> {
    return this._http.delete(`${this._url}${path}`, {headers: this.headers})
      .map(this.checkForError)
      .catch(err => Observable.throw(err))
      .map(this.getJson);
  }

}
